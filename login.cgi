#!/usr/bin/perl

# $Id: login.cgi,v 1.10 2006/12/22 08:58:50 rmurray Exp $
# (c) 1999 Randolph Chung. Licensed under the GPL. <tausq@debian.org>
# (c) 2006 Ryan Murray. Licensed under the GPL. <rmurray@debian.org>
# Copyright (c) 2008, 2011, 2015 Peter Palfrader

use lib '.';
use strict;
#use Apache::Registry;
use CGI;
use Util;
use URI::Escape;
use Net::LDAP qw(LDAP_SUCCESS LDAP_PROTOCOL_ERROR);

my %config = &Util::ReadConfigFile;

my $query = new CGI;
my $proto = ($ENV{HTTPS} ? "https" : "http");

if ($proto eq "http" || !($query->param('username')) || !($query->param('password'))) {
  print "Location: https://$ENV{SERVER_NAME}/$config{webloginhtml}\n\n";
  exit;
}

my $ldap = Net::LDAP->new($config{ldaphost}) || &Util::HTMLError($!);
&Util::UpgradeConnection($ldap) unless $config{usessl} eq 'False';

my $username = $query->param('username');
my $password = $query->param('password');
my $binddn = "uid=$username,$config{basedn}";

my $mesg = $ldap->bind($binddn, password => $password);
$mesg->sync;

if ($mesg->code == LDAP_SUCCESS) {
  # HACK HACK HACK
  # Check for md5 password, and update as necessary
  $mesg = $ldap->search(base   => $config{basedn}, 
                        filter => "(uid=$username)");
  $mesg->code && &Util::HTMLError($mesg->error);
  my $entries = $mesg->as_struct;
  my $dn = (keys %$entries)[0];
  my $oldpassword = $entries->{$dn}->{userpassword}->[0];
  if ($oldpassword !~ /^{crypt}\$1\$/) {
    # Update their password to md5
    open (LOG, ">>$config{weblogfile}");
    print LOG scalar(localtime);
    print LOG ": Updating MD5 password for $dn\n";
    close LOG;
    my $newpassword = '{crypt}'.crypt($password, &Util::CreateCryptSalt(1));
    &Util::LDAPUpdate($ldap, $dn, 'userPassword', $newpassword);
  }
  ## END HACK HACK HACK
  
  my $authtoken = &Util::SavePasswordToFile($username, $password);

  if ($query->param('update')) {
    my $url = "$proto://$ENV{SERVER_NAME}/$config{webupdateurl}?id=$username;authtoken=$authtoken";
    print "Location: $url\n\n";
  } else {
    my $url = "$proto://$ENV{SERVER_NAME}/$config{websearchurl}?id=$username;authtoken=$authtoken";
    print "Location: $url\n\n";
  }

  $ldap->unbind;
} else {
  print "Content-type: text/html; charset=utf-8\n\n";
  print "<html><body><h1>Not authenticated</h1></body></html>\n";
}

